/*
 * typedefs.h
 *
 *  Created on: Sep 10, 2019
 *      Author: lorenzo
 */

#ifndef TYPEDEFS_H_
#define TYPEDEFS_H_

#include <stdint.h>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;

#undef bool
typedef _Bool bool;
#define false 0
#define true 1

#endif /* TYPEDEFS_H_ */
