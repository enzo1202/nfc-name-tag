/*
 * epd.c
 *
 *  Created on: Aug 23, 2019
 *      Author: lorenzo
 */

#include "epd.h"

static void reset() {
	epdRST(false);
	HAL_Delay(200);
	epdRST(true);
	HAL_Delay(200);
	epdRST(false);
	HAL_Delay(200);
}

static void sendCommand(u8 reg) {
	epdDC(false);
//	epdCS(true);
	epdTxByte(reg);
//	epdCS(false);
}

static void sendData(u8 data) {
	epdDC(true);
//	epdCS(true);
	epdTxByte(data);
//	epdCS(false);
}

static void blockBusy() {
	while (epdBusy()) {
		HAL_Delay(100);
	}
}

static void displayOn() {
	sendCommand(0x12);
	HAL_Delay(100);
	blockBusy();
}

void epdInit() {
//	epdHwInit();

	reset();

	sendCommand(0x06);
	sendData(0x17);
	sendData(0x17);
	sendData(0x17);
	sendCommand(0x04);
	blockBusy();
	sendCommand(0x00);
	sendData(0x0F);
}

void epdClear() {
	u16 row, col;

	sendCommand(0x10);
	for (row = 0; row < EPD_HEIGHT; row++) {
		for (col = 0; col < EPD_WIDTH_BYTES; col++) {
			sendData(0xFF);
		}
	}

	sendCommand(0x13);
	for (row = 0; row < EPD_HEIGHT; row++) {
		for (col = 0; col < EPD_WIDTH_BYTES; col++) {
			sendData(0xFF);
		}
	}

	displayOn();
}

void epdImage(const u8 *blackImage, const u8 *redImage) {
	u16 row, col;

	sendCommand(0x10);
	for (row = 0; row < EPD_HEIGHT; row++) {
		for (col = 0; col < EPD_WIDTH_BYTES; col++) {
			sendData(blackImage[col + (row * EPD_WIDTH_BYTES)]);
		}
	}

	sendCommand(0x13);
	for (row = 0; row < EPD_HEIGHT; row++) {
		for (col = 0; col < EPD_WIDTH_BYTES; col++) {
			sendData(redImage[col + (row * EPD_WIDTH_BYTES)]);
		}
	}

	displayOn();
}

void epdPattern(u8 blackPattern, u8 redPattern) {
	u16 row, col;

	sendCommand(0x10);
	for (row = 0; row < EPD_HEIGHT; row++) {
		for (col = 0; col < EPD_WIDTH_BYTES; col++) {
			sendData(blackPattern);
		}
	}

	sendCommand(0x13);
	for (row = 0; row < EPD_HEIGHT; row++) {
		for (col = 0; col < EPD_WIDTH_BYTES; col++) {
			sendData(redPattern);
		}
	}

	displayOn();
}

void epdSleep() {
	sendCommand(0x02);
	blockBusy();
	sendCommand(0x07);
	sendData(0xA5);
}
