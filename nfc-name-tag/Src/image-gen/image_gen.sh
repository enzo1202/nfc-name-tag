#!/bin/bash

# Configure as necessary
X_RES=400
Y_RES=300
BORDER=0
NAME_FONT_SIZE=72
SCHOOL_FONT_SIZE=84
NAME_FONT="MyriadPro-Semibold"
SCHOOL_FONT="MinionPro-Semibold"
NAME="Lorenzo Castoldi"
SCHOOL="WPI"


# Create images (PBM format: 1 bit per pixel, but with header)
convert -size ${X_RES}x${Y_RES} -depth 1 -background white -fill black -bordercolor black -negate -border $BORDER -pointsize $NAME_FONT_SIZE -font $NAME_FONT caption:"$NAME" bImage.pbm
convert -size ${X_RES}x${Y_RES} -depth 1 -background white -fill black -bordercolor black -negate -border $BORDER -gravity SouthWest -pointsize $SCHOOL_FONT_SIZE -font $SCHOOL_FONT caption:"$SCHOOL" rImage.pbm

# Extract last 15000 bytes (400*300 = 120000 bits = 15000 bytes)
tail --bytes 15000 bImage.pbm > bImage.bin
tail --bytes 15000 rImage.pbm > rImage.bin

# Convert bin files to C headers
xxd -i bImage.bin bImage.h
xxd -i rImage.bin rImage.h

# Add "const" keyword to both headers, so image stays in flash (never in memory)
sed -i '1s/^/const /' bImage.h
sed -i '1s/^/const /' rImage.h
