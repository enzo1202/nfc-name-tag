/*
 * epd_hw.h
 *
 *  Created on: Aug 23, 2019
 *      Author: lorenzo
 */

// Functions for tx/rx to the e-paper display
// Need to change/rewrite these functions if hardware changes
// (e.g. different USCI module/package pins, or different microcontroller)

#ifndef EPD_HW_H_
#define EPD_HW_H_

#include <stdint.h>
#include "stm32l4xx_hal.h"
#include "typedefs.h"
#include "main.h"

void epdCS(bool assert);  // true asserts CS (pin low)
void epdRST(bool reset);  // true asserts RST
void epdDC(bool isData);  // true = data, false = command
bool epdBusy();  // true = busy
void epdTxByte(u8 data);

#endif /* EPD_HW_H_ */
