/*
 * epd_hw.c
 *
 *  Created on: Aug 23, 2019
 *      Author: lorenzo
 */

#include "epd/epd_hw.h"

extern SPI_HandleTypeDef hspi1;

void epdCS(bool assert) {
	if (assert) {
		HAL_GPIO_WritePin(EPD_SPI_CS_GPIO_Port, EPD_SPI_CS_Pin, GPIO_PIN_RESET);
	} else {
		HAL_GPIO_WritePin(EPD_SPI_CS_GPIO_Port, EPD_SPI_CS_Pin, GPIO_PIN_SET);
	}
}

void epdRST(bool reset) {
	if (reset) {
		HAL_GPIO_WritePin(EPD_RST_GPIO_Port, EPD_RST_Pin, GPIO_PIN_RESET);

	} else {
		HAL_GPIO_WritePin(EPD_RST_GPIO_Port, EPD_RST_Pin, GPIO_PIN_SET);
	}
}

void epdDC(bool isData) {
	if (isData) {
		HAL_GPIO_WritePin(EPD_DC_GPIO_Port, EPD_DC_Pin, GPIO_PIN_SET);
	} else {
		HAL_GPIO_WritePin(EPD_DC_GPIO_Port, EPD_DC_Pin, GPIO_PIN_RESET);
	}
}

bool epdBusy() {
	if (HAL_GPIO_ReadPin(EPD_busy_GPIO_Port, EPD_busy_Pin)) {
		return false;
	} else {
		return true;
	}
}

void epdTxByte(u8 data) {
	HAL_SPI_Transmit(&hspi1, &data, 1, UINT32_MAX);
	// Blocks until tx finished
}
