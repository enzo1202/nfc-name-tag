/*
 * nfc.c
 *
 *  Created on: Nov 18, 2019
 *      Author: lorenzo
 */

#include "nfc.h"

static rfalNfcDepBufFormat nfcDepTxBuf; // NFC-DEP Rx buffer format (with header/prologue)
static rfalNfcDepBufFormat nfcDepRxBuf; // NFC-DEP Rx buffer format (with header/prologue)
static rfalNfcDepDevice nfcDepDev;  // NFC-DEP Device details

// Listen mode stuff
static rfalBitRate bitRate;  // Needed for listen check/init
static uint8_t hdrLen;

static u8 NFCID3[] = { 0x01, 0xFE, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09,
		0x0A };
static u8 GB[] = { 0x46, 0x66, 0x6d, 0x01, 0x01, 0x11, 0x02, 0x02, 0x07, 0x80,
		0x03, 0x02, 0x00, 0x03, 0x04, 0x01, 0x32, 0x07, 0x01, 0x03 };

// Copied from main.c
// Run once at init
void startRfal() {
	/* Initalize RFAL */
	rfalAnalogConfigInitialize();
	if (rfalInitialize() != ERR_NONE) {
		/*
		 * in case the rfal initalization failed signal it by flashing all LED
		 * and stoping all operations
		 */
		platformLog("RFAL initialization failed..\r\n");
		while (1) {
			platformLedToogle(PLATFORM_LED_FIELD_PORT, PLATFORM_LED_FIELD_PIN);
			platformLedToogle(PLATFORM_LED_A_PORT, PLATFORM_LED_A_PIN);
			platformLedToogle(PLATFORM_LED_B_PORT, PLATFORM_LED_B_PIN);
			platformLedToogle(PLATFORM_LED_F_PORT, PLATFORM_LED_F_PIN);
			platformLedToogle(PLATFORM_LED_V_PORT, PLATFORM_LED_V_PIN);
			platformLedToogle(PLATFORM_LED_AP2P_PORT, PLATFORM_LED_AP2P_PIN);
			platformDelay(100);
		}
	} else {
		platformLog("RFAL initialization succeeded..\r\n");
		// No more of das blinkenlightsen
//		for (int i = 0; i < 6; i++) {
//			platformLedToogle(PLATFORM_LED_FIELD_PORT, PLATFORM_LED_FIELD_PIN);
//			platformLedToogle(PLATFORM_LED_A_PORT, PLATFORM_LED_A_PIN);
//			platformLedToogle(PLATFORM_LED_B_PORT, PLATFORM_LED_B_PIN);
//			platformLedToogle(PLATFORM_LED_F_PORT, PLATFORM_LED_F_PIN);
//			platformLedToogle(PLATFORM_LED_V_PORT, PLATFORM_LED_V_PIN);
//			platformLedToogle(PLATFORM_LED_AP2P_PORT, PLATFORM_LED_AP2P_PIN);
//			platformDelay(200);
//		}

		platformLedOff(PLATFORM_LED_A_PORT, PLATFORM_LED_A_PIN);
		platformLedOff(PLATFORM_LED_B_PORT, PLATFORM_LED_B_PIN);
		platformLedOff(PLATFORM_LED_F_PORT, PLATFORM_LED_F_PIN);
		platformLedOff(PLATFORM_LED_V_PORT, PLATFORM_LED_V_PIN);
		platformLedOff(PLATFORM_LED_AP2P_PORT, PLATFORM_LED_AP2P_PIN);
		platformLedOff(PLATFORM_LED_FIELD_PORT, PLATFORM_LED_FIELD_PIN);
	}

	rfalWorker();  // just in case
}

// Copied from demo.c state machine
void wakeupRfal() {
	rfalWakeUpModeStart(NULL);
	do {  // Block until awake
		rfalWorker();
	} while (!rfalWakeUpModeHasWoke());
	rfalWakeUpModeStop();
}

// Copied from demo.c state machine
void sleepRfal() {
	platformLedOff(PLATFORM_LED_A_PORT, PLATFORM_LED_A_PIN);
	platformLedOff(PLATFORM_LED_B_PORT, PLATFORM_LED_B_PIN);
	platformLedOff(PLATFORM_LED_F_PORT, PLATFORM_LED_F_PIN);
	platformLedOff(PLATFORM_LED_V_PORT, PLATFORM_LED_V_PIN);
	platformLedOff(PLATFORM_LED_AP2P_PORT, PLATFORM_LED_AP2P_PIN);
	platformLedOff(PLATFORM_LED_FIELD_PORT, PLATFORM_LED_FIELD_PIN);

	rfalFieldOff();
	rfalWakeUpModeStop();

	rfalWorker();
}

// Copied from demo.c [was demoPollAP2P()]
// Return true: AP2P device found
// Return false: no device found
bool pollAP2P() {
	ReturnCode err;
	bool try106 = false;

	while (!try106) {
		/*******************************************************************************/
		/* NFC_ACTIVE_POLL_MODE                                                        */
		/*******************************************************************************/
		/* Initialize RFAL as AP2P Initiator NFC BR 424 */
		err = rfalSetMode(RFAL_MODE_POLL_ACTIVE_P2P,
				((try106) ? RFAL_BR_106 : RFAL_BR_424),
				((try106) ? RFAL_BR_106 : RFAL_BR_424));

		if (err != ERR_NONE) {
			return false;
		}

		rfalSetErrorHandling(RFAL_ERRORHANDLING_NFC);
		rfalSetFDTListen(RFAL_FDT_LISTEN_AP2P_POLLER);
		rfalSetFDTPoll(RFAL_TIMING_NONE);

		rfalSetGT(RFAL_GT_AP2P_ADJUSTED);
		err = rfalFieldOnAndStartGT();

		err = activateP2P(NFCID3, RFAL_NFCDEP_NFCID3_LEN, true, &nfcDepDev);
		if (err == ERR_NONE) {
			platformLog("NFC Active P2P device found. NFCID3: %s\r\n",
					hex2Str( nfcDepDev.activation.Target.ATR_RES.NFCID3, RFAL_NFCDEP_NFCID3_LEN));
			platformLedOn(PLATFORM_LED_AP2P_PORT, PLATFORM_LED_AP2P_PIN);
//			rfalWorker();
			return true;
		}

		/* AP2P at 424kb/s didn't found any device, try at 106kb/s */
		try106 = true;
		rfalFieldOff();
	}

	return false;
}

// Copied from demo.c [was demoActivateP2P()]
// Used in pollAP2P to "wake up" a target device
ReturnCode activateP2P(uint8_t* nfcid, uint8_t nfidLen, bool isActive,
		rfalNfcDepDevice *nfcDepDev) {
	rfalNfcDepAtrParam nfcDepParams;

	nfcDepParams.nfcid = nfcid;
	nfcDepParams.nfcidLen = nfidLen;
	nfcDepParams.BS = RFAL_NFCDEP_Bx_NO_HIGH_BR;
	nfcDepParams.BR = RFAL_NFCDEP_Bx_NO_HIGH_BR;
	nfcDepParams.LR = RFAL_NFCDEP_LR_254;
	nfcDepParams.DID = RFAL_NFCDEP_DID_NO;
	nfcDepParams.NAD = RFAL_NFCDEP_NAD_NO;
	nfcDepParams.GBLen = sizeof(GB);
	nfcDepParams.GB = GB;
	nfcDepParams.commMode = (
			(isActive) ? RFAL_NFCDEP_COMM_ACTIVE : RFAL_NFCDEP_COMM_PASSIVE);
	nfcDepParams.operParam = (RFAL_NFCDEP_OPER_FULL_MI_EN
			| RFAL_NFCDEP_OPER_EMPTY_DEP_DIS | RFAL_NFCDEP_OPER_ATN_EN
			| RFAL_NFCDEP_OPER_RTOX_REQ_EN);

	/* Initialize NFC-DEP protocol layer */
	rfalNfcDepInitialize();

	/* Handle NFC-DEP Activation (ATR_REQ and PSL_REQ if applicable) */
	return rfalNfcDepInitiatorHandleActivation(&nfcDepParams, RFAL_BR_424,
			nfcDepDev);
}

// Copied from demo.c [was demoNfcDepBlockingTxRx()]
// rxActLen is "actual" length of received data (inf field length)
ReturnCode nfcTxRx(const uint8_t *txBuf, uint16_t txBufSize, bool isChaining) {
	ReturnCode err;
	rfalNfcDepTxRxParam rfalNfcDepTxRx;

	/* Initialize the NFC-DEP protocol transceive context */
	rfalNfcDepTxRx.txBuf = &nfcDepTxBuf;
	rfalNfcDepTxRx.txBufLen = txBufSize;
	rfalNfcDepTxRx.rxBuf = &nfcDepRxBuf;
	rfalNfcDepTxRx.rxLen = &rxLen;
	rfalNfcDepTxRx.DID = RFAL_NFCDEP_DID_NO;
	rfalNfcDepTxRx.FSx = rfalNfcDepLR2FS(
			rfalNfcDepPP2LR(nfcDepDev.activation.Target.ATR_RES.PPt));
	rfalNfcDepTxRx.FWT = nfcDepDev.info.FWT;
	rfalNfcDepTxRx.dFWT = nfcDepDev.info.dFWT;
	rfalNfcDepTxRx.isRxChaining = &isRxChaining;
	rfalNfcDepTxRx.isTxChaining = isChaining;

	/* Copy data to send */
	ST_MEMCPY(nfcDepTxBuf.inf, txBuf,
			MIN(txBufSize, RFAL_NFCDEP_FRAME_SIZE_MAX_LEN));

	/* Perform the NFC-DEP Transceive in a blocking way */
	rfalNfcDepStartTransceive(&rfalNfcDepTxRx);
	do {
		rfalWorker();
		err = rfalNfcDepGetTransceiveStatus();
	} while (err == ERR_BUSY);

//	platformLog(" NFC-DEP TxRx %s: - Tx: %s Rx: %s \r\n", (err != ERR_NONE) ? "FAIL": "OK", hex2Str( (uint8_t*)rfalNfcDepTxRx.txBuf, txBufSize), (err != ERR_NONE) ? "": hex2Str( rfalNfcDepTxRx.rxBuf->inf, *rxActLen));

	if (err != ERR_NONE) {
		return err;
	}

	// Leave in gRxBuf, use a different function to assemble payload from inf field
//	/* Copy received data */
//	ST_MEMMOVE(rxBuf, gRxBuf.nfcDepRxBuf.inf, MIN(*rxActLen, rxBufSize));
	return ERR_NONE;
}

ReturnCode keepAlive() {
	ReturnCode ret;

	platformLog("Keep alive");
	ret = nfcTxRx(ndefSymm, sizeof(ndefSymm), false);

	while (ret == ERR_NONE) {
		platformLog(".");
		HAL_Delay(50);
		ret = nfcTxRx(ndefSymm, sizeof(ndefSymm), false);
	}
	platformLog("\r\n");

	platformLog("Connection lost, error %d\r\n", ret);
	return ret;
}

ReturnCode nfcTxChain(const u8 *txBuf, u32 txBufSize) {
	u32 fullBlocks = txBufSize / BLOCK_SZ_MAX;
	ReturnCode ret;
	for (u32 i = 0; i < fullBlocks; i++) {
		ret = nfcTxRx(txBuf + (i * BLOCK_SZ_MAX), BLOCK_SZ_MAX, true);
		if (ret != ERR_NONE) {
			platformLog("Fail during chaining, error %d\r\n", ret);
			return ret;
		} else {
			platformLog("Chained block %d success, %d bytes received\r\n", i,
					rxLen);
		}
	}

	u8 sizeLeft = txBufSize - (fullBlocks * BLOCK_SZ_MAX);
	if (sizeLeft > 0) {
		ret = nfcTxRx(txBuf + (fullBlocks * BLOCK_SZ_MAX), sizeLeft, false);
		if (ret != ERR_NONE) {
			platformLog("Fail end of chain, error %d\r\n", ret);
			return ret;
		} else {
			platformLog("End block of chain sent successfully\r\n");
		}
	} else {
		platformLog("No end block needed\r\n");
	}

	platformLog("Chained Tx success\r\n");
	return ret;
}

// Allocate memory for local VCF, plus LLCP/SNEP/NDEF headers, then assemble in
//     VcfTxBuffer
// Only call once!
void makeVcfTxBuf() {
	u32 payloadSize = lorenzoVcfLen;
	const char *payloadType = "text/x-vcard";
	const u8 *payload = lorenzoVcf;

	u8 LLCP_DSAP = 0x04 & 0x3F;
	u8 LLCP_PTYPE = 0xC & 0xF;
	u8 LLCP_SSAP = 0x20 & 0x3F;
	u8 LLCP_NS = 0 & 0xF;
	u8 LLCP_NR = 0 & 0xF;

	u8 SNEP_MajorVersion = 1 & 0xF;
	u8 SNEP_MinorVersion = 0 & 0xF;
	u8 SNEP_request = 0x02;
	u32 SNEP_infoLength;

	bool NDEF_messageStart = true;
	bool NDEF_messageEnd = true;
	bool NDEF_chunk = false;
	bool NDEF_shortRecord;
	bool NDEF_idLengthPresent = false;
	u8 NDEF_TNF = 0x2 & 0x7;
	u8 NDEF_typeLength = strlen(payloadType);
	u32 NDEF_payloadSize = payloadSize;
	u8 NDEF_idLength = 0;
	const char *NDEF_payloadType = payloadType;
	const char *NDEF_id = "";
	NDEF_shortRecord = (NDEF_payloadSize < 0xFF) ? true : false;
	const u8 *NDEF_payload = payload;

	u8 LLCP_headerSize = 3;
	u8 SNEP_headerSize = 6;
	u8 NDEF_headerSize = 2 + (NDEF_shortRecord ? 1 : 4)
			+ (NDEF_idLengthPresent ? 1 : 0) + NDEF_typeLength + NDEF_idLength;
	SNEP_infoLength = NDEF_headerSize + NDEF_payloadSize;

	VcfTxBufferLen = LLCP_headerSize + SNEP_headerSize + NDEF_headerSize
			+ NDEF_payloadSize;
	VcfTxBuffer = malloc(VcfTxBufferLen);

	u32 i = 0;

	// LLCP
	*(VcfTxBuffer + i++) = (LLCP_DSAP << 2) | (LLCP_PTYPE >> 2);
	*(VcfTxBuffer + i++) = (LLCP_PTYPE << 6) | (LLCP_SSAP);
	*(VcfTxBuffer + i++) = (LLCP_NS << 4) | (LLCP_NR);

	// SNEP
	*(VcfTxBuffer + i++) = (SNEP_MajorVersion << 4) | (SNEP_MinorVersion);
	*(VcfTxBuffer + i++) = SNEP_request;
	*(VcfTxBuffer + i++) = (SNEP_infoLength >> 24) & 0xFF;
	*(VcfTxBuffer + i++) = (SNEP_infoLength >> 16) & 0xFF;
	*(VcfTxBuffer + i++) = (SNEP_infoLength >> 8) & 0xFF;
	*(VcfTxBuffer + i++) = (SNEP_infoLength >> 0) & 0xFF;

	// NDEF
	*(VcfTxBuffer + i++) = (NDEF_messageStart ? 1 << 7 : 0)
			| (NDEF_messageEnd ? 1 << 6 : 0) | (NDEF_chunk ? 1 << 5 : 0)
			| (NDEF_shortRecord ? 1 << 4 : 0)
			| (NDEF_idLengthPresent ? 1 << 3 : 0) | NDEF_TNF;
	*(VcfTxBuffer + i++) = NDEF_typeLength;
	if (NDEF_shortRecord) {
		*(VcfTxBuffer + i++) = NDEF_payloadSize;
	} else {
		*(VcfTxBuffer + i++) = (NDEF_payloadSize >> 24) & 0xFF;
		*(VcfTxBuffer + i++) = (NDEF_payloadSize >> 16) & 0xFF;
		*(VcfTxBuffer + i++) = (NDEF_payloadSize >> 8) & 0xFF;
		*(VcfTxBuffer + i++) = (NDEF_payloadSize >> 0) & 0xFF;
	}
	if (NDEF_idLengthPresent) {
		*(VcfTxBuffer + i++) = NDEF_idLength;
	}
	memmove(VcfTxBuffer + i, NDEF_payloadType, NDEF_typeLength);
	i += NDEF_typeLength;
	memmove(VcfTxBuffer + i, NDEF_id, NDEF_idLength);
	i += NDEF_idLength;
	//		*(txData + i++) = initialByte;
	memmove(VcfTxBuffer + i, NDEF_payload, NDEF_payloadSize);
}

// From demoListen state machine (demo.c, listen mode version)
ReturnCode startListen() {
	ReturnCode ret;

	rfalFieldOff();

	// XXX Length changed to length in bits
	// Demo buf len changed to nfcdep max len (maybe should be frame length?)
	// Prologue pointer passed for rx buffer (maybe should be inf field?)
	ret = rfalListenStart(RFAL_LM_MASK_ACTIVE_P2P, NULL, NULL, NULL,
			nfcDepRxBuf.prologue, RFAL_NFCDEP_LEN_MAX * 8, &rxLen); // Length in bits... 0.o (also changed demo buf len to nfcdep len)

//	bool dataFlag;
//	// XXX Block until out of power-off mode
//	while (RFAL_LM_STATE_POWER_OFF == rfalListenGetState(&dataFlag, &bitRate)) {
//		rfalWorker();
//	}
	return ret;
}

// Directly call RFAL library function
// Function is just for naming consistency
ReturnCode stopListen() {
	ReturnCode ret;
	ret = rfalListenStop();

	if (ret == ERR_NONE) {
		rfalWorker();
	}
	return ret;
}

// From demoListen state machine (demo.c, listen mode version)
// Just check if listen mode has found an initiator; don't send anything yet
bool checkListen() {
	bool dataFlag;
	rfalLmState lmSt;

	hdrLen = RFAL_NFCDEP_LEN_LEN;

//	lmSt = rfalListenGetState(&dataFlag, &bitRate); /* Check if Initator has sent some data */
//	return (lmSt == RFAL_LM_STATE_IDLE) && dataFlag;

	rfalWorker();
	lmSt = rfalListenGetState(&dataFlag, &bitRate); /* Check if Initator has sent some data */

	if (lmSt == RFAL_LM_STATE_IDLE && dataFlag) {
		/* SB Byte only in NFC-A */
		if (bitRate == RFAL_BR_106) {
			hdrLen += RFAL_NFCDEP_SB_LEN;
		}

		if (rfalNfcDepIsAtrReq(nfcDepRxBuf.prologue + hdrLen,
				(rfalConvBitsToBytes(rxLen) - hdrLen), NULL)) {
			return true;
		}
	}
	return false;
}

ReturnCode activateListen() {
	ReturnCode ret;
	rfalNfcDepTargetParam param;
	rfalNfcDepListenActvParam rxParam;

	rfalListenSetState(
			(RFAL_BR_106 == bitRate) ?
					RFAL_LM_STATE_TARGET_A : RFAL_LM_STATE_TARGET_F);
	rfalSetMode(RFAL_MODE_LISTEN_ACTIVE_P2P, bitRate, bitRate);

	platformLedOn(PLATFORM_LED_AP2P_PORT, PLATFORM_LED_AP2P_PIN);
	platformLog("Activated as AP2P listener device \r\n");

	ST_MEMCPY(param.nfcid3, NFCID3, RFAL_NFCDEP_NFCID3_LEN);
	param.bst = RFAL_NFCDEP_Bx_NO_HIGH_BR;
	param.brt = RFAL_NFCDEP_Bx_NO_HIGH_BR;
	param.to = RFAL_NFCDEP_WT_TRG_MAX_D11;
	param.ppt = (RFAL_NFCDEP_LR_254 << RFAL_NFCDEP_PP_LR_SHIFT);
	param.GBtLen = 0;
	param.operParam = (RFAL_NFCDEP_OPER_FULL_MI_DIS
			| RFAL_NFCDEP_OPER_EMPTY_DEP_EN | RFAL_NFCDEP_OPER_ATN_EN
			| RFAL_NFCDEP_OPER_RTOX_REQ_EN);
	param.commMode = RFAL_NFCDEP_COMM_ACTIVE;

	rxParam.rxBuf = &nfcDepRxBuf;
	rxParam.rxLen = &rxLen;
	rxParam.isRxChaining = &isRxChaining;
	rxParam.nfcDepDev = &nfcDepDev;

	/* ATR_REQ received, trigger NFC-DEP layer to handle activation (sends ATR_RES and handles PSL_REQ)  */
	ret = rfalNfcDepListenStartActivation(&param, nfcDepRxBuf.prologue + hdrLen,
			(rfalConvBitsToBytes(rxLen) - hdrLen), rxParam);

	if (ret != ERR_NONE) {
		platformLog("ATR_REQ fail, error %d\r\n", ret);
		return ret;
	} else {
		platformLog("ATR_REQ success\r\n");
	}

	// NFC-DEP activation
	// Different branch in state machine
	do {
		rfalWorker();
		ret = rfalNfcDepListenGetActivationStatus();
	} while (ret == ERR_BUSY);

	return ret;
}

// Not really sure what this does or how I figured out we need it
// Gets stuck in ERR_BUSY loop during TxRx if not used after activation
ReturnCode blockingRx() {
	ReturnCode ret;

	do {
		rfalWorker();
		ret = rfalNfcDepGetTransceiveStatus();
	} while (ret == ERR_BUSY);
	return ret;
}

// Send a SYMM, then receive an n-block payload
// Extracts the payload from the header stuff and stores in txBuf (NOT the rfal buf)
// Total size of assembled payload is returned in txBufSize
ReturnCode nfcRxChain(u8 *rxBuf, size_t *rxSize) {
	ReturnCode ret;
	u8 *ndefReader;  // used to "step through" nfcdep buffer (i.e. over headers)
	u32 payloadLen;  // from NDEF header, should match rxBufOffset at end
	u32 rxBufOffset = 0;

	// Send SYMM, get first payload block
	ret = nfcTxRx(ndefSymm, sizeof(ndefSymm), false);
	if (ret != ERR_NONE && ret != ERR_AGAIN) {
		platformLog("    SYMM fail, error %d\r\n", ret);
		return ret;
	} else {
		platformLog("    SYMM success,     %d bytes received, block 0\r\n",
				rxLen);
	}

	// Skip LLCP/SNEP/NDEF headers
	ndefReader = nfcDepRxBuf.inf;
	ndefReader += 3;  // skip LLCP
	ndefReader += 6;  // skip SNEP (incl. info field length)
	bool idLengthPresent = (*ndefReader & (1 << 3)) ? true : false;
	bool shortRecord = (*ndefReader & (1 << 4)) ? true : false;
	ndefReader++;
	uint8_t typeLen = *ndefReader;
	ndefReader++;
	if (shortRecord) {
		payloadLen = *ndefReader++;
	} else {
		payloadLen = (*ndefReader++);
		payloadLen = payloadLen << 8;
		payloadLen |= (*ndefReader++);
		payloadLen = payloadLen << 8;
		payloadLen |= (*ndefReader++);
		payloadLen = payloadLen << 8;
		payloadLen |= (*ndefReader++);
	}
	if (idLengthPresent) {
		ndefReader++;
	}
	ndefReader += typeLen;  // ndefReader now points to beginning of payload

	size_t header_crap = ndefReader - nfcDepRxBuf.inf;
	size_t copyLen = rxLen - header_crap;
	memcpy(rxBuf + rxBufOffset, ndefReader, copyLen);
	rxBufOffset += copyLen;

	// Send nothing, get rest of payload [n-1 blocks]
	while (isRxChaining) {
		static u16 i = 1;
		ret = blockingRx();
		if (ret != ERR_NONE && ret != ERR_AGAIN) {
			platformLog("    Blocking fail, error %d\r\n", ret);
			return ret;
		} else {
			platformLog("    Blocking success, %d bytes received, block %d\r\n",
					rxLen, i++);
		}
		memcpy(rxBuf + rxBufOffset, nfcDepRxBuf.inf, rxLen);
		rxBufOffset += rxLen;
	}

	if (payloadLen != rxBufOffset) {
		platformLog("    payloadLen (%d) and rxBufOffset (%d) mismatch\r\n",
				payloadLen, rxBufOffset);
	}
	*rxSize = payloadLen;
	return ret;
}
