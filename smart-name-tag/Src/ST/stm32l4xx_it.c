/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file    stm32l4xx_it.c
 * @brief   Interrupt Service Routines.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include <filesystem/fs_ops.h>
#include <flash/commands.h>
#include <ST/stm32l4xx_it.h>
#include "main.h"
#include "st25r3911_interrupt.h"
#include "epd/epd.h"
#include "image-gen/images.h"
#include "nfc.h"
// Begin USB MSC code
// End USB MSC code
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern PCD_HandleTypeDef hpcd_USB_FS;
extern LPTIM_HandleTypeDef hlptim1;
extern TIM_HandleTypeDef htim7;
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/******************************************************************************/
/*           Cortex-M4 Processor Interruption and Exception Handlers          */
/******************************************************************************/
/**
 * @brief This function handles Non maskable interrupt.
 */
void NMI_Handler(void)
{
	/* USER CODE BEGIN NonMaskableInt_IRQn 0 */

	/* USER CODE END NonMaskableInt_IRQn 0 */
	/* USER CODE BEGIN NonMaskableInt_IRQn 1 */

	/* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
 * @brief This function handles Hard fault interrupt.
 */
void HardFault_Handler(void)
{
	/* USER CODE BEGIN HardFault_IRQn 0 */

	/* USER CODE END HardFault_IRQn 0 */
	while (1)
	{
		/* USER CODE BEGIN W1_HardFault_IRQn 0 */
		/* USER CODE END W1_HardFault_IRQn 0 */
	}
}

/**
 * @brief This function handles Memory management fault.
 */
void MemManage_Handler(void)
{
	/* USER CODE BEGIN MemoryManagement_IRQn 0 */

	/* USER CODE END MemoryManagement_IRQn 0 */
	while (1)
	{
		/* USER CODE BEGIN W1_MemoryManagement_IRQn 0 */
		/* USER CODE END W1_MemoryManagement_IRQn 0 */
	}
}

/**
 * @brief This function handles Prefetch fault, memory access fault.
 */
void BusFault_Handler(void)
{
	/* USER CODE BEGIN BusFault_IRQn 0 */

	/* USER CODE END BusFault_IRQn 0 */
	while (1)
	{
		/* USER CODE BEGIN W1_BusFault_IRQn 0 */
		/* USER CODE END W1_BusFault_IRQn 0 */
	}
}

/**
 * @brief This function handles Undefined instruction or illegal state.
 */
void UsageFault_Handler(void)
{
	/* USER CODE BEGIN UsageFault_IRQn 0 */

	/* USER CODE END UsageFault_IRQn 0 */
	while (1)
	{
		/* USER CODE BEGIN W1_UsageFault_IRQn 0 */
		/* USER CODE END W1_UsageFault_IRQn 0 */
	}
}

/**
 * @brief This function handles System service call via SWI instruction.
 */
void SVC_Handler(void)
{
	/* USER CODE BEGIN SVCall_IRQn 0 */

	/* USER CODE END SVCall_IRQn 0 */
	/* USER CODE BEGIN SVCall_IRQn 1 */

	/* USER CODE END SVCall_IRQn 1 */
}

/**
 * @brief This function handles Debug monitor.
 */
void DebugMon_Handler(void)
{
	/* USER CODE BEGIN DebugMonitor_IRQn 0 */

	/* USER CODE END DebugMonitor_IRQn 0 */
	/* USER CODE BEGIN DebugMonitor_IRQn 1 */

	/* USER CODE END DebugMonitor_IRQn 1 */
}

/**
 * @brief This function handles Pendable request for system service.
 */
void PendSV_Handler(void)
{
	/* USER CODE BEGIN PendSV_IRQn 0 */

	/* USER CODE END PendSV_IRQn 0 */
	/* USER CODE BEGIN PendSV_IRQn 1 */

	/* USER CODE END PendSV_IRQn 1 */
}

/**
 * @brief This function handles System tick timer.
 */
void SysTick_Handler(void)
{
	/* USER CODE BEGIN SysTick_IRQn 0 */

	/* USER CODE END SysTick_IRQn 0 */
	HAL_IncTick();
	/* USER CODE BEGIN SysTick_IRQn 1 */

	/* USER CODE END SysTick_IRQn 1 */
}

/******************************************************************************/
/* STM32L4xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32l4xx.s).                    */
/******************************************************************************/

/**
 * @brief This function handles EXTI line0 interrupt.
 */
void EXTI0_IRQHandler(void)
{
	/* USER CODE BEGIN EXTI0_IRQn 0 */
	/* USER CODE END EXTI0_IRQn 0 */
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
	/* USER CODE BEGIN EXTI0_IRQn 1 */
	st25r3911Isr();  // from RFAL, don't mess with
	// 'Getting' interrupt clears it if present--only do this when no function
	//     is active (TxRx/activate listen all need interrupts while running)
//	if (listenModeActive && !transceiveActive) {
	if (waitingForField)
	{
		if (st25r3911CheckInterrupt(ST25R3911_IRQ_MASK_EON))
		{ // don't reset flag
			// TxRx logic needs this IRQ to preempt it, so we can't call
			//     nfcTargetLogic() here. Instead, call EXTI1, which is lower
			//     priority. Starts execution after IRQ exits, and gets
			//     preempted by the IRQ
			EXTI->SWIER1 |= 1 << 1;  // set software interrupt
			waitingForField = false; // don't do this part again until txrx is done
		}
	}
	/* USER CODE END EXTI0_IRQn 1 */
}

/**
 * @brief This function handles EXTI line1 interrupt.
 */
void EXTI1_IRQHandler(void)
{
	/* USER CODE BEGIN EXTI1_IRQn 0 */
	HAL_LPTIM_SetOnce_Stop_IT(&hlptim1);  // stop initiator interrupt timer
	/* USER CODE END EXTI1_IRQn 0 */
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);
	/* USER CODE BEGIN EXTI1_IRQn 1 */
	// Run target logic at lower priority than EXTI0 (peripheral IRQ line)
	ReturnCode ret;
	FS_RESULT res;
	u32 startTick = HAL_GetTick();
	while (HAL_GetTick() - startTick < 2000)
	{
		ret = nfcTargetLogic();
		if (ret != ERR_NOT_ACTIVATED)
		{ // spam until we get a hit, 2 second timeout
			break;
		}
	}

	// Good xfer, but not necessarily successful until we clear FS with no problems
	if (ret == ERR_NONE)
	{
		pauseUSB();
		res = writeVcf();
		resumeUSB();
	}

	if (ret == ERR_RELEASE_REQ)
	{ // OnePlus 7 pro activates tag, causes this error
		waitingForField = false; // don't get activated as target until we get a turn as initiator
	}
	else if (ret == ERR_NOT_ACTIVATED)
	{ // didn't get activated--must have been my imagination...
		waitingForField = true;  // business as usual
	}
	else
	{  // actual nametag xfer, either good or failed
		if ((ret == ERR_NONE) && (res == FS_OK))
		{
#ifndef SUPPRESS_EPD
			// Display "Success" on EPD (partial refresh in corner)
			epdSendTile(NULL, succ, EPD_WIDTH - succ_w, EPD_HEIGHT - succ_h, succ_w, succ_h, true);
#endif
		}
		else
		{  // bad transfer
#ifndef SUPPRESS_EPD
			// Display "Failure" on EPD (partial refresh in corner)
			epdSendTile(NULL, fail, EPD_WIDTH - fail_w, EPD_HEIGHT - fail_h, fail_w, fail_h, true);
#endif
		}
#ifndef SUPPRESS_EPD
		HAL_Delay(2000);  // give user time to read

		// Put normal stuff back with full refresh
//	epdPattern(0xFF, 0xFF, false);
//	epdSendTile(topText, NULL, 0, 0, topText_w, topText_h, false);
//	epdSendTile(NULL, bottomText, 0, EPD_HEIGHT - bottomText_h,
//			bottomText_w, bottomText_h, false);
//	epdRefresh();
		epdDispName();
#endif
		waitingForField = true;
	}

// Re-enable initiator interrupts
	if (ret == ERR_RELEASE_REQ)
	{  // Smartphone--wake up as initiator ASAP
		HAL_LPTIM_SetOnce_Start_IT(&hlptim1, 0, 0xFFFF);
	}
	else
	{
		// Reset timer to full
		// TODO seed rand with user's contact file?
		u32 extraTime = rand() % (((LISTENMODE_DUR * 256) - 1) / 2);// prevent all tags in venue from "synchronizing"
		HAL_LPTIM_SetOnce_Start_IT(&hlptim1, (LISTENMODE_DUR * 256) - 1 + extraTime, 0xFFFF);
	}
	/* USER CODE END EXTI1_IRQn 1 */
}

/**
 * @brief This function handles TIM7 global interrupt.
 */
void TIM7_IRQHandler(void)
{
	/* USER CODE BEGIN TIM7_IRQn 0 */
// Begin USB MSC code
	if (bufferAddress != NO_SECTOR_IN_BUF)
	{
		timeSinceLastWriteBack += 100;
	}
	if (timeSinceLastWriteBack == 1000)
	{
		writeBack();
	}
// End USB MSC code
	/* USER CODE END TIM7_IRQn 0 */
	HAL_TIM_IRQHandler(&htim7);
	/* USER CODE BEGIN TIM7_IRQn 1 */
	/* USER CODE END TIM7_IRQn 1 */
}

/**
 * @brief This function handles LPTIM1 global interrupt.
 */
void LPTIM1_IRQHandler(void)
{
	/* USER CODE BEGIN LPTIM1_IRQn 0 */
	waitingForField = false;
	HAL_LPTIM_SetOnce_Stop_IT(&hlptim1);
	/* USER CODE END LPTIM1_IRQn 0 */
	HAL_LPTIM_IRQHandler(&hlptim1);
	/* USER CODE BEGIN LPTIM1_IRQn 1 */
	ReturnCode ret;
	ret = nfcInitiatorLogic();

	if (ret == ERR_NONE)
	{
		pauseUSB();
		writeVcf();
		resumeUSB();
	}

	if (ret != ERR_NOTHING_FOUND)
	{ // *something* happened, not just "no nametag found"
		if (ret == ERR_NONE)
		{  // successful nametag TxRx
#ifndef SUPPRESS_EPD
			epdSendTile(NULL, succ, EPD_WIDTH - succ_w, EPD_HEIGHT - succ_h, succ_w, succ_h, true);
#endif
		}
		else if (ret == ERR_SMARTPHONE_FOUND)
		{  // Tx only to a smartphone
#ifndef SUPPRESS_EPD
			// TODO can do something else here if we want to (display "smartphone", etc)
			epdSendTile(NULL, succ, EPD_WIDTH - succ_w, EPD_HEIGHT - succ_h, succ_w, succ_h, true);
#endif
		}
		else
		{
#ifndef SUPPRESS_EPD
			epdSendTile(NULL, fail, EPD_WIDTH - fail_w, EPD_HEIGHT - fail_h, fail_w, fail_h, true);
#endif
		}
		HAL_Delay(2000);  // give user time to read

#ifndef SUPPRESS_EPD
		// Put normal stuff back with full refresh
//		epdPattern(0xFF, 0xFF, false);
//		epdSendTile(topText, NULL, 0, 0, topText_w, topText_h, false);
//		epdSendTile(NULL, bottomText, 0, EPD_HEIGHT - bottomText_h,
//				bottomText_w, bottomText_h, false);
//		epdRefresh();
		epdDispName();
#endif
	}

	waitingForField = true;
	HAL_LPTIM_SetOnce_Start_IT(&hlptim1, (LISTENMODE_DUR * 256) - 1, 0xFFFF);
	/* USER CODE END LPTIM1_IRQn 1 */
}

/**
 * @brief This function handles USB event interrupt through EXTI line 17.
 */
void USB_IRQHandler(void)
{
	/* USER CODE BEGIN USB_IRQn 0 */

	/* USER CODE END USB_IRQn 0 */
	HAL_PCD_IRQHandler(&hpcd_USB_FS);
	/* USER CODE BEGIN USB_IRQn 1 */

	/* USER CODE END USB_IRQn 1 */
}

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
