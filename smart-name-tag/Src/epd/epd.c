/*
 * epd.c
 *
 *  Created on: Aug 23, 2019
 *      Author: lorenzo
 */

#include <filesystem/fs_ops.h>
#include <flash/storage.h>
#include "epd/epd.h"
#include "ff.h"

static void reset()
{
	epdRST(false);
	HAL_Delay(200);
	epdRST(true);
	HAL_Delay(200);
	epdRST(false);
	HAL_Delay(200);
}

static void sendCommand(u8 reg)
{
	epdDC(false);
	epdCS(true);
	epdTxByte(reg);
	epdCS(false);
}

static void sendData(u8 data)
{
	epdDC(true);
	epdCS(true);
	epdTxByte(data);
	epdCS(false);
}

static void blockBusy()
{
	while (epdBusy())
	{
		HAL_Delay(100);
	}
}

void epdInit()
{
	epdRST(false);
	epdDC(true);
	epdCS(false);

	reset();

	sendCommand(BTST_REG);
	sendData(0x17 & BT_PHA_MASK);
	sendData(0x17 & BT_PHB_MASK);
	sendData(0x17 & BT_PHC_MASK);

	sendCommand(PON_REG);
	blockBusy();
	sendCommand(PSR_REG);
	sendData(RES_0 | UD | SHL | SHD_N | RST_N);
}

// Flushes internal epd framebuffer(s) to display
// Takes several seconds, will block if called while EPD is busy
void epdRefresh()
{
	blockBusy();
	sendCommand(DRF_REG);
	HAL_Delay(100);
}

// Clear both framebuffers (set to white, 0xFF), and refresh, clearing the display
// Just in case we forget which bitmask is white ;)
void epdClear()
{
	epdPattern(0xFF, 0xFF, true);
}

// Write a full 400x300 bitmap image to epd framebuffer and refresh to display it
// To leave a framebuffer unchanged, pass a null pointer
void epdImageFullscreen(const u8 *blackImage, const u8 *redImage)
{
	u16 row, col;

	blockBusy();
	if (blackImage != NULL)
	{
		sendCommand(DTM1_REG);
		for (row = 0; row < EPD_HEIGHT; row++)
		{
			for (col = 0; col < EPD_WIDTH_BYTES; col++)
			{
				sendData(blackImage[col + (row * EPD_WIDTH_BYTES)]);
			}
		}
	}

	if (redImage != NULL)
	{
		sendCommand(DTM2_REG);
		for (row = 0; row < EPD_HEIGHT; row++)
		{
			for (col = 0; col < EPD_WIDTH_BYTES; col++)
			{
				sendData(redImage[col + (row * EPD_WIDTH_BYTES)]);
			}
		}
	}

	epdRefresh();
}

// Displays a 8x1 px pattern, repeated across the whole screen
// Writes to framebuffer(s), and optionally refreshes display
void epdPattern(u8 blackPattern, u8 redPattern, bool refresh)
{
	u16 row, col;

	blockBusy();
	sendCommand(DTM1_REG);
	for (row = 0; row < EPD_HEIGHT; row++)
	{
		for (col = 0; col < EPD_WIDTH_BYTES; col++)
		{
			sendData(blackPattern);
		}
	}

	sendCommand(DTM2_REG);
	for (row = 0; row < EPD_HEIGHT; row++)
	{
		for (col = 0; col < EPD_WIDTH_BYTES; col++)
		{
			sendData(redPattern);
		}
	}

	if (refresh)
	{
		epdRefresh();
	}
}

// Send a rectangular "tile" into the epd framebuffer(s)
// Coords and dimensions are in PIXELS, not bytes/banks/etc
// X start coord and width MUST be multiples of 8
// Either execute partial refresh, or do a fullscreen "normal" refresh later
//     to avoid artifacting
int epdSendTile(const u8 *bImage, const u8 *rImage, u16 xStart, u16 yStart, u16 w, u16 h, bool refresh)
{
	if (xStart % 8 != 0 || w % 8 != 0)
	{
		return -1;  // all X dimensions must be multiples of 8
	}

	u16 xEnd = xStart + w - 1;
	u16 yEnd = yStart + h - 1;

	if (xEnd > EPD_WIDTH - 1 || yEnd > EPD_HEIGHT - 1)
	{
		return -1;  // image outside bounds of screen
	}

	blockBusy();
	sendCommand(PTIN_REG);  // enter partial mode

	// Set partial window
	sendCommand(PTL_REG);
	sendData((xStart >> 8) & H_MASK_0);
	sendData(xStart & H_MASK_1);
	sendData((xEnd >> 8) & H_MASK_0);
	sendData((xEnd & H_MASK_1) | 0x07);
	sendData((yStart >> 8) & V_MASK_0);
	sendData(yStart & V_MASK_1);
	sendData((yEnd >> 8) & V_MASK_0);
	sendData(yEnd & V_MASK_1);
//	sendData(PT_SCAN);  // Scan all gates (default)
	sendData(0);  // Only scan affected gates
	// Causes visible artifacting: thin horizontal lines above and below
	//     window, and faint grey filled rectangle from right edge to
	//     ~20px from window's right edge

	u16 wBytes = w / 8;  // bytes per line

	// Black
	if (bImage != NULL)
	{
		sendCommand(DTM1_REG);
		for (u16 row = 0; row < h; row++)
		{
			for (u16 col = 0; col < wBytes; col++)
			{
				sendData(bImage[col + (row * wBytes)]);
			}
		}
	}

	// Red
	if (rImage != NULL)
	{
		sendCommand(DTM2_REG);
		for (u16 row = 0; row < h; row++)
		{
			for (u16 col = 0; col < wBytes; col++)
			{
				sendData(rImage[col + (row * wBytes)]);
			}
		}
	}

	if (refresh)
	{
		epdRefresh();  // partial refresh (i.e. artifacting)
	}

	sendCommand(PTOUT_REG);  // exit partial mode
	return 0;
}

// Display name/organization, read from filesystem (external flash)
void epdDispName()
{
	FS_RESULT res;
	// Initialize these to 0 so we can see if they need freeing.
	u8* top_im = { 0 }, * bot_im = { 0 };

	int dim[4];

	pauseUSB();
	res = loadImages(&top_im, &bot_im, dim);
	resumeUSB();

	int top_w = dim[0];
	int top_h = dim[1];
	int bot_w = dim[2];
	int bot_h = dim[3];

	if (res != FS_OK)
	{
		LOG(FATAL, "Can't display name on EPD, halt.");
		if (top_im)
		{
			free(top_im);
		}
		if (bot_im)
		{
			free(bot_im);
		}
		while (true);
	}
	else
	{
		epdPattern(0xFF, 0xFF, false);
		epdSendTile(NULL, top_im, 0, 0, top_w, top_h, false);
		epdSendTile(NULL, bot_im, 0, EPD_HEIGHT - bot_h, bot_w, bot_h, false);
		epdRefresh();

		free(top_im);
		free(bot_im);
	}
}

void epdSleep()
{
	blockBusy();
	sendCommand(POF_REG);
	blockBusy();
	sendCommand(DSLP_REG);
	sendData(DEEP_SLEEP_CHECK);
}
