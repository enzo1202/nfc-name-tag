/*
 * commands.c
 *
 *  Created on: Dec 9, 2019
 *      Author: admoore
 */

#include <flash/commands.h>

uint16_t getDeviceID(SPI_HandleTypeDef *hspi)
{
	uint8_t txBuf[4] = { 0x90, 0x00, 0x00, 0x00 };
	uint8_t rxBuf[2] = { 0xDE, 0xAD };
	HAL_GPIO_WritePin(nCS, 0);
	HAL_SPI_Transmit(hspi, txBuf, 4, 5000);
	HAL_SPI_Receive(hspi, rxBuf, 2, 5000);
	HAL_GPIO_WritePin(nCS, 1);

	return *((uint16_t*) rxBuf);
}

int isBusy(SPI_HandleTypeDef *hspi)
{
	uint8_t txBuf[1] = { 0x05 };
	uint8_t rxBuf[1] = { 0x0F };
	HAL_GPIO_WritePin(nCS, 0);
	HAL_SPI_Transmit(hspi, txBuf, 1, 5000);
	HAL_SPI_Receive(hspi, rxBuf, 1, 5000);
	HAL_GPIO_WritePin(nCS, 1);
	return (rxBuf[0] & 0x01);
}

uint16_t readStatusRegs(SPI_HandleTypeDef *hspi)
{
	uint8_t txBuf[1] = { 0x05 };
	uint8_t lowByte[1] = { 0x00 };
	uint8_t highByte[1] = { 0x00 };
	uint16_t result = 0;
	HAL_GPIO_WritePin(nCS, 0);
	HAL_SPI_Transmit(hspi, txBuf, 1, 5000);
	HAL_SPI_Receive(hspi, lowByte, 1, 5000);
	HAL_GPIO_WritePin(nCS, 1);
	txBuf[0] = 0x35;
	HAL_GPIO_WritePin(nCS, 0);
	HAL_SPI_Transmit(hspi, txBuf, 1, 5000);
	HAL_SPI_Receive(hspi, highByte, 1, 5000);
	HAL_GPIO_WritePin(nCS, 1);

	result = (highByte[0] << 8) | lowByte[0];

	return result;
}

void writeEnable(SPI_HandleTypeDef *hspi)
{
	uint8_t txBuf[1] = { 0x06 };
	HAL_GPIO_WritePin(nCS, 0);
	HAL_SPI_Transmit(hspi, txBuf, 1, 5000);
	HAL_GPIO_WritePin(nCS, 1);
}

void writeDisable(SPI_HandleTypeDef *hspi)
{
	uint8_t txBuf[1] = { 0x04 };
	HAL_GPIO_WritePin(nCS, 0);
	HAL_SPI_Transmit(hspi, txBuf, 1, 5000);
	HAL_GPIO_WritePin(nCS, 1);
}

int isWriteEnabled(SPI_HandleTypeDef *hspi)
{
	uint8_t txBuf[1] = { 0x05 };
	uint8_t rxBuf[1] = { 0x00 };
	HAL_GPIO_WritePin(nCS, 0);
	HAL_SPI_Transmit(hspi, txBuf, 1, 5000);
	HAL_SPI_Receive(hspi, rxBuf, 1, 5000);
	HAL_GPIO_WritePin(nCS, 1);

	if (rxBuf[0] & 0x02)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void pollUntilNotBusy(SPI_HandleTypeDef *hspi)
{
	uint8_t txBuf[1] = { 0x05 };
	uint8_t rxBuf[1] = { 0xFF };
	int busy = 1;
	HAL_GPIO_WritePin(nCS, 0);
	HAL_SPI_Transmit(hspi, txBuf, 1, 5000);
	while (busy)
	{
		HAL_SPI_Receive(hspi, rxBuf, 1, 5000);
		busy = (rxBuf[0] & 0x01);
	}
	HAL_GPIO_WritePin(nCS, 1);
}

void readData(SPI_HandleTypeDef *hspi, uint8_t *buf, uint32_t addr, uint32_t n_bytes)
{
	uint8_t* addr_bytes = (uint8_t*) &addr;
	uint8_t txBuf[4] = { 0x03, addr_bytes[2], addr_bytes[1], addr_bytes[0] };
	HAL_GPIO_WritePin(nCS, 0);
	HAL_SPI_Transmit(hspi, txBuf, 4, 5000);
	HAL_SPI_Receive(hspi, buf, n_bytes, 5000);
	HAL_GPIO_WritePin(nCS, 1);
}

void fastReadData(SPI_HandleTypeDef *hspi, uint8_t *buf, uint32_t addr, uint32_t n_bytes)
{
	uint8_t* addr_bytes = (uint8_t*) &addr;
	uint8_t txBuf[5] = { 0x0B, addr_bytes[2], addr_bytes[1], addr_bytes[0], 0x00 };
	HAL_GPIO_WritePin(nCS, 0);
	HAL_SPI_Transmit(hspi, txBuf, 5, 5000);
	HAL_SPI_Receive(hspi, buf, n_bytes, 5000);
	HAL_GPIO_WritePin(nCS, 1);
}

void pageProgram(SPI_HandleTypeDef *hspi, uint8_t *buf, uint32_t addr, uint32_t n_bytes)
{
	addr = addr & PAGE_BIT_MASK; // Always program on a fresh page.

	if (n_bytes > PAGE_SIZE) // Can't program more than one page.
	{
		return;
	}

	uint8_t* addr_bytes = (uint8_t*) &addr;
	uint8_t command[4] = { 0x02, addr_bytes[2], addr_bytes[1], addr_bytes[0] };
	uint8_t* txBuf = (uint8_t*) calloc(260, sizeof(uint8_t));
	memcpy((void*) txBuf, (void*) command, 4);
	memcpy((void*) txBuf + 4, (void*) buf, n_bytes);
	HAL_GPIO_WritePin(nCS, 0);
	HAL_SPI_Transmit(hspi, txBuf, 4 + n_bytes, 5000);
	HAL_GPIO_WritePin(nCS, 1);
	free(txBuf);
}

void sectorErase(SPI_HandleTypeDef *hspi, uint32_t addr)
{
	addr &= SECTOR_BIT_MASK; // Start address of sector.
	uint8_t* addr_bytes = (uint8_t*) &addr;
	uint8_t txBuf[4] = { 0x20, addr_bytes[2], addr_bytes[1], addr_bytes[0] };
	HAL_GPIO_WritePin(nCS, 0);
	HAL_SPI_Transmit(hspi, txBuf, 4, 5000);
	HAL_GPIO_WritePin(nCS, 1);
}

void chipErase(SPI_HandleTypeDef *hspi)
{
	uint8_t txBuf[1] = { 0x60 };
	HAL_GPIO_WritePin(nCS, 0);
	HAL_SPI_Transmit(hspi, txBuf, 1, 5000);
	HAL_GPIO_WritePin(nCS, 1);
}

void enableReset(SPI_HandleTypeDef *hspi)
{
	uint8_t txBuf[1] = { 0x66 };
	HAL_GPIO_WritePin(nCS, 0);
	HAL_SPI_Transmit(hspi, txBuf, 1, 5000);
	HAL_GPIO_WritePin(nCS, 1);
}

void resetChip(SPI_HandleTypeDef *hspi)
{
	uint8_t txBuf[1] = { 0x99 };
	HAL_GPIO_WritePin(nCS, 0);
	HAL_SPI_Transmit(hspi, txBuf, 1, 5000);
	HAL_GPIO_WritePin(nCS, 1);
}
