/*
 * storage.c
 *
 *  Created on: Jan 10, 2020
 *      Author: admoore
 */

#include <flash/storage.h>

uint8_t* sectorBuffer;
uint32_t bufferAddress;
uint32_t timeSinceLastWriteBack;

FLASH_STATUS flash_isReady()
{
	if(isBusy(HSPI))
	{
		return CHIP_BUSY;
	}
	else
	{
		uint16_t device_ID = getDeviceID(HSPI);
		if (device_ID != 0x13EF)
		{
			LOG(ERROR, "Chip unreachable.");
			return CHIP_DISCONNECTED;
		}
		else
		{
			return CHIP_OK;
		}
	}
}

void flash_getCapacity(uint32_t *block_num, uint16_t *block_size)
{
	*block_num = NUM_BLOCKS;
	*block_size = BLOCK_SIZE;
}

void flash_read(uint8_t lun, uint8_t *buf, uint32_t blk_addr, uint16_t blk_len)
{
	if (lun != 0)
	{
		LOG(WARNING, "Incorrect LUN number given to flash_read(). Returning.");
		return;
	}

	uint32_t addr = blk_addr * BLOCK_SIZE;
	uint32_t first_sector_addr = addr & SECTOR_BIT_MASK;
	uint32_t n_bytes = blk_len * BLOCK_SIZE;
	uint32_t last_sector_addr = (addr + n_bytes) & SECTOR_BIT_MASK;

	// Read region contains the buffered sector, must write back first.
	if ((first_sector_addr <= bufferAddress) && (bufferAddress <= last_sector_addr))
	{
		writeBack();
	}

	LOG(TRACE, "Reading %d block(s) from address 0x%05lX.", blk_len, addr);
	readBlocks(buf, addr, blk_len);
}

void flash_write(uint8_t lun, const uint8_t *buf, uint32_t blk_addr, uint16_t blk_len)
{
	if (lun != 0)
	{
		LOG(WARNING, "Incorrect LUN number given to flash_write(). Returning.");
		return;
	}

	uint32_t start_addr = blk_addr * BLOCK_SIZE;

	LOG(TRACE, "Writing %d block(s) to address 0x%05lX.", blk_len, start_addr);

	for (uint16_t i = 0; i < blk_len; i++)
	{
		const uint8_t* data = buf + i * BLOCK_SIZE;
		uint32_t addr = start_addr + i * BLOCK_SIZE;
		writeBlock(data, addr);
	}
}

void writeBlock(const uint8_t *buf, uint32_t addr)
{
	uint32_t sector_addr = addr & SECTOR_BIT_MASK;
	uint32_t offset = addr - (addr & SECTOR_BIT_MASK);
	if (sector_addr == bufferAddress) // We are writing to the sector currently stored in the buffer.
	{
		LOG(TRACE, "Copying block to sector buffer.");
		memcpy(sectorBuffer + offset, buf, BLOCK_SIZE); // Copy the block to the desired location in the buffer.
	}
	else if (bufferAddress == NO_SECTOR_IN_BUF) // No need to write back because the buffer is empty.
	{
		LOG(TRACE, "Sector buffer empty, bringing in new sector.");
		bufferAddress = sector_addr; // Set the new buffer address.
		readBlocks(sectorBuffer, sector_addr, BLOCKS_IN_SECTOR); // Copy the sector from the chip.
		// We erase the sector now to reduce the amount of time spent in pollUntilNotBusy();
		eraseSector(sector_addr);
		memcpy(sectorBuffer + offset, buf, BLOCK_SIZE);
	}
	else // We must first write back the sector currently stored in the buffer.
	{
		writeBack();

		bufferAddress = sector_addr; // Set the new buffer address.
		readBlocks(sectorBuffer, sector_addr, BLOCKS_IN_SECTOR); // Copy the sector from the chip.
		// We erase the sector now to reduce the amount of time spent in pollUntilNotBusy();
		eraseSector(sector_addr);
		memcpy(sectorBuffer + offset, buf, BLOCK_SIZE);
	}
}

void writeBack()
{
	LOG(TRACE, "Writing back sector buffer.");
	if (bufferAddress == NO_SECTOR_IN_BUF)
	{
		LOG(TRACE, "No sector in buffer, nothing to do")
				return;
	}
	writeSector(sectorBuffer, bufferAddress);
	bufferAddress = NO_SECTOR_IN_BUF; // Mark buffer as empty.
	timeSinceLastWriteBack = 0;
}

void readBlocks(uint8_t *buf, uint32_t addr, uint32_t n_blocks)
{
	if (!buf)
	{
		LOG(ERROR, "Null pointer passed in to writePages(). Returning.");
		return;
	}

	if (addr > MAX_ADDRESS)
	{
		LOG(WARNING, "Address passed in to writeSector() [0x%05lX] outside of bounds. Returning.", addr);
		return;
	}
	HAL_GPIO_WritePin(DISK_LED, 1);
	pollUntilNotBusy(HSPI);
	fastReadData(HSPI, buf, addr, n_blocks * BLOCK_SIZE);
	HAL_GPIO_WritePin(DISK_LED, 0);
}

void eraseSector(uint32_t addr)
{
	if (addr > MAX_ADDRESS)
	{
		LOG(WARNING, "Address passed in to writeSector() [0x%05lX] outside of bounds. Returning.", addr);
		return;
	}
	HAL_GPIO_WritePin(DISK_LED, 1);
	pollUntilNotBusy(HSPI);
	writeEnable(HSPI);
	sectorErase(HSPI, addr);
	LOG(TRACE, "Erased sector starting at address 0x%05lX.", addr);
	HAL_GPIO_WritePin(DISK_LED, 0);
}

void writeSector(uint8_t *buf, uint32_t addr)
{
	if (!buf)
	{
		LOG(ERROR, "Null pointer passed in to writeSector(). Returning.");
		return;
	}

	addr = addr & SECTOR_BIT_MASK;

	if (addr > MAX_ADDRESS)
	{
		LOG(WARNING, "Address passed in to writeSector() [0x%05lX] outside of bounds. Returning.", addr);
		return;
	}

	LOG(TRACE, "Writing sector at address 0x%05lX.", addr);
	HAL_GPIO_WritePin(DISK_LED, 1);
	for (int i = 0; i < PAGES_IN_SECTOR; i++)
	{
		pollUntilNotBusy(HSPI);
		writeEnable(HSPI);
		uint32_t offset = i * PAGE_SIZE;
		pageProgram(HSPI, buf + offset, addr + offset, PAGE_SIZE);
	}
	HAL_GPIO_WritePin(DISK_LED, 0);
}

void eraseChip()
{
	pollUntilNotBusy(HSPI);
	HAL_GPIO_WritePin(DISK_LED, 1);
	writeEnable(HSPI);
	chipErase(HSPI);
	HAL_GPIO_WritePin(DISK_LED, 0);
}

void pauseUSB()
{
	HAL_NVIC_DisableIRQ(USB_IRQn);
}

void resumeUSB()
{
	writeBack();
	HAL_NVIC_EnableIRQ(USB_IRQn);
}

void initFlash()
{
	LOG(INFO, "Initializing flash...");

	HAL_GPIO_WritePin(Flash_WP_GPIO_Port, Flash_WP_Pin, 1);
	HAL_GPIO_WritePin(Flash_HOLD_GPIO_Port, Flash_HOLD_Pin, 1);
	HAL_TIM_Base_Start_IT(&htim7);

	sectorBuffer = (uint8_t*) calloc(SECTOR_SIZE, sizeof(uint8_t*));
	bufferAddress = NO_SECTOR_IN_BUF;
	timeSinceLastWriteBack = 0;

	if (flash_isReady() == CHIP_DISCONNECTED)
	{
		LOG(FATAL, "Fail. Flash chip unreachable, spin forever.");
		while (1);
	}
	else
	{
		LOG(INFO, "Done");
	}

#ifdef DEVBOARD
	if (HAL_GPIO_ReadPin(USER_BTN_GPIO_Port, USER_BTN_Pin))
#else
	if (!HAL_GPIO_ReadPin(But_1_GPIO_Port, But_1_Pin))
#endif
	{
		LOG(INFO, "Clearing flash...");
		eraseChip();
		LOG(INFO, "Done");
	}
}

