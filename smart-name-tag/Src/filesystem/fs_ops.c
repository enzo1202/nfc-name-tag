/*
 * fsOps.c
 *
 *  Created on: Mar 1, 2020
 *      Author: lorenzo
 */

#include <filesystem/fatfs.h>
#include <filesystem/fs_ops.h>
#include <flash/storage.h>
#include <stdio.h>
#include "main.h"
#include "typedefs.h"
#include "st_errno.h"
#include "nfc.h"
#include "logger.h"
#include "text.h"

FS_RESULT createFS()
{
	LOG(INFO, "Initializing file system");

	FS_RESULT res;

	// No loop, just for error breaking.
	do
	{
		// Mount or create the file system.
		res = mount();
		CHECK(res);

		// Change into the home directory.
		res = home();
		CHECK(res);

		// Create the README file.
		res = write("README.TXT", (u8*) usage_txt, strlen(usage_txt));
		CHECK(res);

		res = write("LICENSE.TXT", (u8*) license_txt, strlen(license_txt));
		CHECK(res);

		res = chdir("CONTACTS");
		CHECK(res);

		res = home();
		CHECK(res);

		res = chdir("IMAGES");
		CHECK(res);

		// In /IMAGES; create resolution file template if not present
		// Attempt to open the file.
		FRESULT fres = f_open(&USERFile, "RES.TXT", FA_OPEN_EXISTING);
		switch (fres)
		{
			case FR_NO_FILE: // File does not exist, create it.
				LOG(INFO, "RES.TXT does not exist. Creating...");
				char* str = "TOP_W=1\r\nTOP_H=2\r\nBOT_W=3\r\nBOT_H=4\r\n";
				res = write("RES.TXT", (u8*) str, strlen(str));
				break;
			case FR_OK:
				LOG(INFO, "RES.TXT exists, not modified\r\n");
				fres = f_close(&USERFile);
				if (fres == FR_OK)
				{
					res = FS_OK;
				}
				else
				{
					res = FS_CLOSE_ERR;
				}
				break;
			default:
				res = FS_OPEN_ERR;
		}

		CHECK(res);

		res = home();
		CHECK(res);
	} while (false);

	LOG(INFO, "File system initialization complete.");
	return res;
}

// Free images in epdDispName.
FS_RESULT loadImages(u8 **top_im, u8 **bot_im, int *dim)
{

	FS_RESULT res;

	int top_w, top_h, bot_w, bot_h;

	char* res_str = NULL;

	do
	{
		// Check for null pointers
		if (!(top_im && bot_im && dim))
		{
			break;
		}

		res = chdir("images");
		CHECK(res);

		// Read resolutions from res.txt.
		FSIZE_t f_size = 0;
		res = fsize("res.txt", &f_size);
		CHECK(res);

		res_str = malloc(f_size);

		// Read from the file.
		res = read("res.txt", (u8*) res_str, f_size);
		CHECK(res);

		// Scan the string.
		sscanf(res_str, "TOP_W=%d TOP_H=%d BOT_W=%d BOT_H=%d", &top_w, &top_h, &bot_w, &bot_h);

		// Fill out the dimension array, for returning.
		dim[0] = top_w;
		dim[1] = top_h;
		dim[2] = bot_w;
		dim[3] = bot_h;

		// Read top image, check file size against resolutions from file.
		res = fsize("top_im.bin", &f_size);
		CHECK(res);

		if (f_size != (top_w / 8) * top_h)
		{
			LOG(ERROR, "Top image dimensions don't match file size.");
			res = FS_MISMATCHED_DIM;
			break;
		}

		*top_im = malloc(f_size);

		res = read("top_im.bin", *top_im, f_size);
		CHECK(res);

		// Read bottom image, check filesize against resolutions from file
		res = fsize("bot_im.bin", &f_size);
		CHECK(res);

		if (f_size != (bot_w / 8) * bot_h)
		{
			LOG(ERROR, "Bottom image dimensions don't match file size.");
			res = FS_MISMATCHED_DIM;
			break;
		}

		*bot_im = malloc(f_size);

		res = read("bot_im.bin", *bot_im, f_size);
		CHECK(res);

		res = home();
		CHECK(res);

	} while (false);

	if (res_str)
	{
		free(res_str);
	}

	return res;
}

FS_RESULT loadVCF(u8 **buf, uint *size)
{
	FS_RESULT res;
	FSIZE_t f_size;

	do
	{
		res = fsize("my_info.vcf", &f_size);
		CHECK(res);

		*buf = malloc(f_size);  // Freed in makeVcfTxBuf()

		res = read("my_info.vcf", *buf, f_size);
		CHECK(res);

		*size = (uint) f_size;

	} while (false);

	return res;
}

FS_RESULT writeVcf()
{
	FS_RESULT res;
	FRESULT fres;
	u16 vcfIndex = 0;  // suffix for VCF files on FS (e.g. 23 for CON00023.VCF)
	char filename[13];  // 13 chars, enough for 8.3 plus null

	do
	{  // no loop, just break on errors

		res = chdir("contacts");
		CHECK(res);

		// Find the first file that doesn't exist
		do
		{
			snprintf(filename, 13, "CON%05d.VCF", vcfIndex);
			fres = f_stat(filename, NULL);
			if (fres == FR_OK)
			{
				if (vcfIndex == 0xFFFFFFFF)
				{
					LOG(ERROR, "No space in file system.");
					res = FS_FULL;
					break;
				}
				else
				{
					vcfIndex++;
				}
			}
		} while (fres != FR_NO_FILE);

		res = write(filename, VcfRxBuffer, VcfRxSize);
		CHECK(res);

		res = home();
		CHECK(res);

	} while (false);
	return res;
}

FS_RESULT mount()
{
	uint8_t res = f_mount(&USERFatFS, USERPath, 1);

	if (res == FR_OK)
	{
		LOG(INFO, "Successfully mounted file system.");
		return FS_OK;
	}
	else
	{
		// Try to create a file system
		LOG(ERROR, "Failed to mount file system. Creating one.");
		uint8_t* buf = (uint8_t*) calloc(8 * SECTOR_SIZE, sizeof(uint8_t)); // Allocate 32KB of memory for the working buffer.
		res = f_mkfs(USERPath, FM_FAT, BLOCK_SIZE, buf, 8 * SECTOR_SIZE);
		if (res == FR_OK)
		{
			LOG(INFO, "Successfully created file system.");
			free(buf);
			res = f_mount(&USERFatFS, USERPath, 1);
			if (res == FR_OK)
			{
				LOG(INFO, "Successfully mounted file system.");
				return FS_OK;
			}
			else
			{
				LOG(ERROR, "Failed to mount created file system [error %d].", res);
				return FS_MOUNT_ERR;
			}
		}
		else // mkfs() failed.
		{
			free(buf);
			LOG(ERROR, "Failed to create file system [error %d].", res);
			return FS_MKFS_ERR;
		}
	}
}

FS_RESULT chdir(const char *path)
{
	FRESULT res;
	res = f_chdir(path);
	if (res == FR_OK)
	{
		LOG(INFO, "Changed directories into %s.", path);
		return FS_OK;
	}
	else
	{
		LOG(ERROR, "Could not change directories into %s [error %d], attempting to create...", path, res);
		res = f_mkdir(path);
		if (res == FR_OK)
		{
			LOG(INFO, "Successfully created %s directory.", path);
			res = f_chdir(path);
			if (res == FR_OK)
			{
				LOG(INFO, "Changed into new directory.");
				return FS_DIR_CREATED;
			}
			else
			{
				LOG(ERROR, "Could not change into new directory [error %d].", res);
				return FS_DIR_CHANGE_ERR;
			}
		}
		else // mkdir failed.
		{
			LOG(ERROR, "Could not create directory [error %d].", res);
			return FS_DIR_CREATE_ERR;
		}
	}
}

FS_RESULT home()
{
	FRESULT res;
	res = f_chdir("/");
	if (res == FR_OK)
	{
		return FS_OK;
	}
	else
	{
		LOG(ERROR, "Could not change into home directory [error %d].", res);
		return FS_DIR_CHANGE_ERR;
	}
}

FS_RESULT fsize(const char *path, FSIZE_t *size)
{
	FILINFO info;
	FRESULT res = f_stat(path, &info);
	if (res == FR_OK)
	{
		*size = info.fsize;
		return FS_OK;
	}
	else
	{
		LOG(ERROR, "Could not get status of %s [error %d].", path, res);
		return FS_STAT_ERR;
	}
}

FS_RESULT write(const char *path, const u8 *buf, u16 size)
{
	LOG(INFO, "Writing to %s...", path);
	FRESULT res = f_open(&USERFile, path, FA_CREATE_ALWAYS | FA_WRITE);
	if (res == FR_OK)
	{
		uint bytes_written = 0;
		// We can ignore the return code of f_write. If there is a problem, it is shown that bytes_written != size.
		f_write(&USERFile, buf, size, &bytes_written);

		res = f_close(&USERFile);
		if (res == FR_OK)
		{
			if (bytes_written == size)
			{
				LOG(INFO, "Write complete.");
				return FS_OK;
			}
			else
			{
				LOG(WARNING, "Full write not completed [expected %d bytes, wrote %d bytes].", size, bytes_written);
				return FS_WRITE_ERR;
			}
		}
		else // Close failed.
		{
			LOG(ERROR, "Could not close file after writing [error %d].", res);
			return FS_CLOSE_ERR;
		}
	}
	else // Open failed.
	{
		LOG(ERROR, "Could not open %s for writing [error %d].", path, res);
		return FS_OPEN_ERR;
	}
}

FS_RESULT read(const char *path, u8 *buf, FSIZE_t size)
{
	FRESULT res = f_open(&USERFile, path, FA_READ);
	if(res == FR_OK)
	{
		uint bytes_read = 0;

		// WE can ignore this return code, because if an error occurs, bytes_read will be 0.
		f_read(&USERFile, buf, size, &bytes_read);

		res = f_close(&USERFile);
		if (res == FR_OK)
		{
			if (bytes_read == size)
			{
				return FS_OK;
			}
			else
			{
				LOG(WARNING, "Full read not completed [expected %ld bytes, wrote %d bytes].", size, bytes_read);
				return FS_READ_ERR;
			}
		}
		else // Close failed.
		{
			LOG(ERROR, "Could not close file after writing [error %d].", res);
			return FS_CLOSE_ERR;
		}
	}
	else // Open failed.
	{
		LOG(ERROR, "Could not open %s for reading [error %d]", path, res);
		return FS_OPEN_ERR;
	}
}
