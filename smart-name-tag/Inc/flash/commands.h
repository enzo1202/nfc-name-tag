/*
 * commands.h
 *
 *  Created on: Dec 9, 2019
 *      Author: admoore
 */

#ifndef INC_COMMANDS_H_
#define INC_COMMANDS_H_

#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "logger.h"
#include "storage.h"

#define nCS Flash_CS_GPIO_Port, Flash_CS_Pin

// Retrieves the manufacturer information from the device and places it into the buffer. The buffer must have 2 bytes available.
uint16_t getDeviceID(SPI_HandleTypeDef *hspi);

// Checks if the chip is busy.
int isBusy(SPI_HandleTypeDef *hspi);

// Reads the two status registers and returns them as an integer.
uint16_t readStatusRegs(SPI_HandleTypeDef *hspi);

// Enables writing to the chip.
void writeEnable(SPI_HandleTypeDef *hspi);

// Disables writing to the chip.
void writeDisable(SPI_HandleTypeDef *hspi);

// Reads the device's status register 1 and returns true if the chip is write-enabled.
int isWriteEnabled(SPI_HandleTypeDef *hspi);

// Reads the status register repeatedly until the busy bit is not set.
void pollUntilNotBusy(SPI_HandleTypeDef *hspi);

// Reads a number of bytes specified by n_bytes starting at the address specified by addr. This data is copied into the buffer.
void readData(SPI_HandleTypeDef *hspi, uint8_t *buf, uint32_t addr,
		uint32_t n_bytes);

// Similar to readData(), but is capable of a faster clock speed (80MHz, instead of 33MHz).
void fastReadData(SPI_HandleTypeDef *hspi, uint8_t *buf, uint32_t addr,
		uint32_t n_bytes);

// Programs a page (256 Bytes) of memory. Can only be called after sector has been erased.
void pageProgram(SPI_HandleTypeDef *hspi, uint8_t *buf, uint32_t addr,
		uint32_t n_bytes);

// Erases a sector (4096 Bytes) of memory. Can only be called when write-enabled.
void sectorErase(SPI_HandleTypeDef *hspi, uint32_t addr);

// Erases the entire chip. Can only be called when write-enabled.
void chipErase(SPI_HandleTypeDef *hspi);

// Enables resetting the chip.
void enableReset(SPI_HandleTypeDef *hspi);

// Resets the chip. Must be preceded by enableReset().
void resetChip(SPI_HandleTypeDef *hspi);

#endif /* INC_COMMANDS_H_ */
