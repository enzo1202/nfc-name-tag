/*
 * storage.h
 *
 *  Created on: Jan 10, 2020
 *      Author: admoore
 */

#ifndef INC_STORAGE_H_
#define INC_STORAGE_H_

#include "commands.h"

#define HSPI &hspi3
#define PAGE_SIZE 256
#define BLOCK_SIZE 512
#define SECTOR_SIZE 4096
#define NUM_PAGES 4096
#define NUM_BLOCKS 2048
#define NUM_SECTORS 256
#define PAGE_BIT_MASK 0xFFFF00
#define BLOCK_BIT_MASK 0xFFFE00
#define SECTOR_BIT_MASK 0xFFF000
#define PAGES_IN_SECTOR SECTOR_SIZE/PAGE_SIZE
#define BLOCKS_IN_SECTOR SECTOR_SIZE/BLOCK_SIZE

#define NO_SECTOR_IN_BUF 0xFFFFFFFF
#define MAX_ADDRESS 0x0FFFFF

#define DISK_LED LED_3_GPIO_Port, LED_3_Pin

typedef enum {
	CHIP_OK = 0,
	CHIP_BUSY,
	CHIP_DISCONNECTED
} FLASH_STATUS;

extern TIM_HandleTypeDef htim7;

// Buffer to store a sector to modify before writing back to the chip.
extern uint8_t *sectorBuffer;

// Starting address of the sector currently in the buffer. This variable is 0xFFFFFFFF if there is no modified sector
// in the buffer.
extern uint32_t bufferAddress;

extern uint32_t timeSinceLastWriteBack;

extern SPI_HandleTypeDef hspi3;

/* flash_getCapacity: Outputs the block size and number of blocks to the two input parameters. The standard
 * block size for most operating systems is 512 bytes.
 *
 * block_num: The total number of blocks in the chip. In this case, this is 2048.
 * block_size: The size of each block. In this case, this is 512 bytes.
 */
void flash_getCapacity(uint32_t *block_num, uint16_t *block_size);

/* flash_isReady: Checks to see if the chip is connected.
 *
 * returns CHIP_OK if the device ID was successfully read.
 * returns CHIP_ERR if the chip is disconnected or unreachable.
 */
FLASH_STATUS flash_isReady();

/* flash_read: Copies a number of blocks from the chip into the buffer specified by buf.
 *
 * This function assumes that the size of buf is greater than or equal to the number of bytes to read. If this is
 * not the case, memory on the MCU might be adversely affected.
 *
 * buf: The buffer to which to copy data.
 * blk_addr: The sequential number of the starting block to read. Note that this is not equivalent to the block
 * 			 address, although the transformation is trivial: the starting address of the block is block_addr *
 * 			 BLOCK_SIZE.
 * blk_len: The number of blocks to copy from the device.
 */
void flash_read(uint8_t lun, uint8_t *buf, uint32_t blk_addr, uint16_t blk_len);

/* flash_write: Write a number of blocks from buf into the chip.
 *
 * This function assumes that the amount of data in buf is greather than or equal to the number of bytes to read.
 * If this is not the case, garbage data may be written to the device.
 *
 * This is the highest level reading function. It takes into account the sector buffer and will write it back
 * if the requested block to read falls within the sector currently in the buffer.
 *
 * buf: The buffer from which to write data.
 * blk_addr: The sequential number of the starting block to write. See flash_isReady() for a more in-depth
 * 			 explanation.
 * blk_len: The number of blocks to write to the device.
 */
void flash_write(uint8_t lun, const uint8_t *buf, uint32_t blk_addr,
		uint16_t blk_len);

/* writeBlock: write a block to the specified address.
 *
 * If the desired block is in the sector that's in the sector buffer, we modify the sector buffer locally. Otherwise,
 * we bring a new sector into the sector buffer and erase it on the chip. If a new sector is desired when the sector buffer
 * is not empty, we first write the buffer back to the chip.
 *
 * buf: The buffer of data to write.
 * addr: The starting address of the block to write.
 */
void writeBlock(const uint8_t *buf, uint32_t addr);

/* writeBack: write the sector currently stored in the sector buffer to its address.
 *
 * This function allows for minimizing the number of writes and erases done on the chip. Any time a write command
 * is issued, the function reads the entire sector into memory, and edits it locally. Any subsequent writes to the
 * same sector are also done locally. Only when a write command is issued for a new sector, or a read is issued for the
 * sector in the buffer, or a certain amount of time has passed, is the sector in the buffer written back into memory.
 * This "writing-back" is the purpose of this function.
 */
void writeBack();

/*******************************************************************************************************************
 * ABSTRACTIONS OF CHIP-LEVEL COMMANDS
 *
 * These functions all make sure the busy bit is cleared before issuing any commands. Therefore, issuing these
 * commands is always safe to do within higher-level blocks of code, even directly after one another. Polling the busy
 * bit at the start of these functions allows the STM32 to execute instructions during the chip's busy time instead of
 * blocking on pollUntilNotBusy().
 ******************************************************************************************************************/

/* readBlocks: Read a specified number of blocks into the buffer specified by buf.
 *
 * This function does not take into account the sector buffer, and only reads data from the chip itself.
 * As implemented, this function is only called in points where we do not care what the sector buffer contains
 * (i.e. immediately after a write back).
 *
 * buf: The buffer of data to write to.
 * addr: The starting address of the first block to read.
 * n_blocks: The number of blocks to read.
 */
void readBlocks(uint8_t *buf, uint32_t addr, uint32_t n_blocks);

/* eraseSector: Sends a sector erase command to the chip with the address specified by addr.
 *
 * When passed into sectorErase(), the address is and-ed with the SECTOR_BIT_MASK. So in theory, passing in any address
 * will erase the sector containing that address. However, all calls to this function currently pass in an address at the
 * start of the sector.
 *
 * addr: The starting address of the sector to erase.
 */
void eraseSector(uint32_t addr);

/* writeSector: Writes a sector's worth of pages to the flash chip.
 *
 * buf: The buffer of data to write (assumed to be at least a sector long)
 * addr: The address of the sector to which to write.
 */
void writeSector(uint8_t *buf, uint32_t addr);

/* eraseChip: erase the entire chip. */
void eraseChip();

void pauseUSB();
void resumeUSB();

// Setup flash chip; run once after reset. Disable USB IRQ before calling.
void initFlash();

#endif /* INC_STORAGE_H_ */
