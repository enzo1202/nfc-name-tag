/*
 * nfc.h
 *
 *  Created on: Nov 18, 2019
 *      Author: lorenzo
 */

#ifndef NFC_H_
#define NFC_H_

#include <stdlib.h>
#include "utils.h"
#include "rfal_rf.h"
#include "rfal_analogConfig.h"
#include "rfal_nfcDep.h"
#include "typedefs.h"
#include "lorenzo.h"
#include <flash/storage.h>

// Time spent in listen mode in seconds (ie approx. time between active polling attempts)
#define LISTENMODE_DUR 1

#define BLOCK_SZ_MAX 61  // 251 used to work before bidirecional rewritten chaining
                         // Biggest size that works now (62 is too big)

#define VCF_MAX_SZ 1024  // Max size of VCF to receive in bytes

// Special return codes for target/initiator logic functions
enum {
	// ST errcodes max out at 47, no overlap
	ERR_SMARTPHONE_FOUND = 48,  // poll activated a smartphone, not a nametag; Rx only completed
	ERR_NOT_ACTIVATED = 49,  // checked for an activator but didn't find one
	ERR_NOTHING_FOUND = 50,  // polled for a target but didn't find one
	ERR_FILESYSTEM = 51  // filesystem problem
};

// Pseudo-static variables (defined only in nfc.c, shared among any files that include this file)
extern bool waitingForField;  // in listen mode, waiting for ext field to wake up
//extern bool listenModeActive;  // true when LM active, false otherwise ("gate" checked in IRQ)
//extern bool transceiveActive;  // true when actively TxRx-ing ("gate" checked in IRQ)
extern bool isRxChaining;
extern u16 rxLen;  // length of last block received in bytes (inf section only?)
extern u8 *VcfTxBuffer;  // Points to dynamically-allocated LLCP~NDEF blob
extern size_t VcfTxBufferLen;
extern u8 VcfRxBuffer[VCF_MAX_SZ];  // Chained rx packets assembled here
extern size_t VcfRxSize;  // Size of received VCF file (NOT any of the Rx buffers)

static const uint8_t ndefSymm[] = { 0x00, 0x00 };
static const uint8_t ndefInit[] = { 0x05, 0x20, 0x06, 0x0F, 0x75, 0x72, 0x6E,
		0x3A, 0x6E, 0x66, 0x63, 0x3A, 0x73, 0x6E, 0x3A, 0x73, 0x6E, 0x65, 0x70,
		0x02, 0x02, 0x07, 0x80, 0x05, 0x01, 0x02 };
static const uint8_t ndefUriSTcom[] = { 0x13, 0x20, 0x00, 0x10, 0x02, 0x00,
		0x00, 0x00, 0x19, 0xc1, 0x01, 0x00, 0x00, 0x00, 0x12, 0x55, 0x00, 0x68,
		0x74, 0x74, 0x70, 0x3a, 0x2f, 0x2f, 0x77, 0x77, 0x77, 0x2e, 0x73, 0x74,
		0x2e, 0x63, 0x6f, 0x6d };
static const size_t ndefSymmSize = sizeof(ndefSymm);
static const size_t ndefInitSize = sizeof(ndefInit);
static const size_t ndefUriSTcomSize = sizeof(ndefUriSTcom);

void startRfal();
void wakeupRfal();
bool pollAP2P(void);
ReturnCode activateP2P(u8* nfcid, u8 nfidLen, bool isActive,
		rfalNfcDepDevice *nfcDepDev);
ReturnCode nfcTxRx(const u8 *txBuf, u16 txBufSize, bool isChaining);
ReturnCode keepAlive();
ReturnCode nfcTxChain(const u8 *txBuf, u32 txBufSize);
void makeVcfTxBuf();

ReturnCode startListen();
ReturnCode stopListen();
bool checkListen();
ReturnCode activateListen();
ReturnCode blockingRx();
ReturnCode nfcRxChain(u8 *rxBuf, size_t *rxSize);

ReturnCode nfcInitiatorLogic();
ReturnCode nfcTargetLogic();

#endif /* NFC_H_ */
