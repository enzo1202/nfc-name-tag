/******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2016 STMicroelectronics</center></h2>
  *
  * Licensed under ST MYLIBERTY SOFTWARE LICENSE AGREEMENT (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/myliberty
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied,
  * AND SPECIFICALLY DISCLAIMING THE IMPLIED WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
******************************************************************************/
/*
 *      PROJECT:   
 *      $Revision: $
 *      LANGUAGE:  ANSI C
 */

/*! \file
 *
 *  \author 
 *
 *  \brief serial output log declaration file
 *
 */
/*!
 *
 * This driver provides a printf-like way to output log messages
 * via the UART interface. It makes use of the uart driver.
 *
 * API:
 * - Write a log message to UART output: #DEBUG
 */

#ifndef LOGGER_H
#define LOGGER_H

/*
******************************************************************************
* INCLUDES
******************************************************************************
*/
#include <stdio.h>  // XXX added to fix(?) vsnprintf in logUsart
#include <ST/platform.h>

/*
******************************************************************************
* DEFINES
******************************************************************************
*/
#define LOGGER_ON   1
#define LOGGER_OFF  0
#define USE_LOGGER LOGGER_ON  // XXX is this the right way to enable logging?

#define FATAL 0
#define ERROR 1
#define WARNING 2
#define INFO 3
#define TRACE 4
#define LOGGER_LEVEL INFO

#define LOG(level, args...) if(LOGGER_LEVEL >= level){ char str[256] = ""; \
							uint8_t offset = printLevel(str, level); \
							offset += sprintf(str + offset, args); \
							offset += sprintf(str + offset, "\r\n"); \
							logUsart(str); }

/*!
 *****************************************************************************
 *  \brief  Writes out a formated string via UART interface
 *
 *  This function is used to write a formated string via the UART interface.
 *
 *****************************************************************************
 */
extern void logUsartInit(UART_HandleTypeDef *husart);

/*!
 *****************************************************************************
 *  \brief  Writes out a formated string via UART interface
 *
 *  This function is used to write a formated string via the UART interface.
 *
 *****************************************************************************
 */
extern int logUsart(const char* format, ...);

/*!
 *****************************************************************************
 *  \brief  helper to convert hex data into formated string
 *
 *  \param[in] data : pointer to buffer to be dumped.
 *
 *  \param[in] dataLen : buffer length
 *
 *  \return hex formated string
 *
 *****************************************************************************
 */
extern char* hex2Str(unsigned char * data, size_t dataLen);

uint8_t printLevel(char *str, uint8_t level);

#endif /* LOGGER_H */

