/*
 * epd.h
 *
 *  Created on: Aug 23, 2019
 *      Author: lorenzo
 */

// "Driver"-style functions to control e-paper display
// No porting needed, just rewrite SPI tx/rx functions
#ifndef EPD_H_
#define EPD_H_

#include "typedefs.h"
#include "epd_hw.h"

#define SUPPRESS_EPD

#define EPD_WIDTH 400
#define EPD_WIDTH_BYTES 50
#define EPD_HEIGHT 300

void epdInit();

void epdRefresh();

void epdClear();

void epdImage(const u8 *blackImage, const u8 *redImage);

void epdPattern(u8 blackPattern, u8 redPattern, bool refresh);

// All start/end coords in px, not bytes
int epdSendTile(const u8 *bImage, const u8 *rImage, u16 xPx, u16 yPx, u16 wPx,
		u16 hPx, bool refresh);

void epdSleep();

void epdDispName();

#endif /* EPD_H_ */
