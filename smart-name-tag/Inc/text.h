/*
 * text.h
 *
 *  Created on: Mar 17, 2020
 *      Author: admoore
 */

#ifndef INC_TEXT_H_
#define INC_TEXT_H_

const char * usage_txt = "How to use the Smart Name Tag:\r\n\r\n"
		"1. Create your own VCF file with your contact information.\r\n"
		"       You can export this from your smartphone's address book,\r\n"
		"       or create one from scratch at http://bvcard.com/vCard-vcf-generator\r\n\r\n"
		"2. Rename the file to `MY_INFO.VCF` (without the quotes),\r\n"
		"       then put it here in the root folder of the name tag.\r\n\r\n"
		"3. Create images for your name tag. You can have a top image (your name),\r\n"
		"       and a bottom image (your organization). The images must be in a simple,\r\n"
		"       header-less `.BIN` format; 1 bit per pixel, 8 pixels per byte.\r\n"
		"       Put these files in the `IMAGES` folder, named `TOP_IM.BIN` and `BOT_IM.BIN,\r\n"
		"       and fill out the 'RES.TXT' file with their resolutions.\r\n"
		"		The width of all images must be a multiple of 8.\r\n"
		"4. Network! To exchange contact information, simply hold two name tags\r\n"
		"       together until the LED lights up solid for a second or so.\r\n"
		"       The screen will display `Success` if all has gone well.\r\n\r\n"
		"5. To access your received contacts, look in the `contacts` folder.\r\n"
		"       You can easily import these with your phone, or an email client such as Outlook.\r\n";

const char * license_txt = "This device uses third-party software, licensed as follows:\r\n\r\n"
		"- The HAL library is licensed by ST under the BSD 3-Clause License,\r\n"
		"      available at opensource.org/licenses/BSD-3-Clause\r\n"
		"      Copyright 2017 STMicroelectronics\r\n"
		"- The ST25R391x firmware is licensed by ST under the ST MYLIBERTY SOFTWARE LICENSE AGREEMENT,\r\n"
		"      available at http://www.st.com/myliberty\r\n"
		"      Copyright 2016 STMicroelectronics\r\n"
		"- The USB MSC firmware is licensed by ST under the Ultimate Liberty license,\r\n"
		"		available at www.st.com/SLA0044\r\n"
		"		Copyright 2015 STMicroelectronics\r\n"
		"- The FatFs open source software\r\n"
		"       Copyright 2017 ChaN\r\n";



#endif /* INC_TEXT_H_ */
