/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */
/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */
/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define USER_BTN_Pin GPIO_PIN_13
#define USER_BTN_GPIO_Port GPIOC
#define nfcLED6TX_Pin GPIO_PIN_0
#define nfcLED6TX_GPIO_Port GPIOC
#define nfcLED5AP2P_Pin GPIO_PIN_1
#define nfcLED5AP2P_GPIO_Port GPIOC
#define LED_3_Pin GPIO_PIN_2
#define LED_3_GPIO_Port GPIOC
#define LED_4_Pin GPIO_PIN_3
#define LED_4_GPIO_Port GPIOC
#define nfcIRQ_Pin GPIO_PIN_0
#define nfcIRQ_GPIO_Port GPIOA
#define nfcIRQ_EXTI_IRQn EXTI0_IRQn
#define bogoIRQ_Pin GPIO_PIN_1
#define bogoIRQ_GPIO_Port GPIOA
#define bogoIRQ_EXTI_IRQn EXTI1_IRQn
#define debugTx_Pin GPIO_PIN_2
#define debugTx_GPIO_Port GPIOA
#define debugRx_Pin GPIO_PIN_3
#define debugRx_GPIO_Port GPIOA
#define EPD_SPI_CS_Pin GPIO_PIN_4
#define EPD_SPI_CS_GPIO_Port GPIOA
#define EPD_SPI_CLK_Pin GPIO_PIN_5
#define EPD_SPI_CLK_GPIO_Port GPIOA
#define EPD_busy_Pin GPIO_PIN_6
#define EPD_busy_GPIO_Port GPIOA
#define EPD_SPI_MOSI_Pin GPIO_PIN_7
#define EPD_SPI_MOSI_GPIO_Port GPIOA
#define EPD_RST_Pin GPIO_PIN_4
#define EPD_RST_GPIO_Port GPIOC
#define EPD_DC_Pin GPIO_PIN_0
#define EPD_DC_GPIO_Port GPIOB
#define nfcCS_Pin GPIO_PIN_12
#define nfcCS_GPIO_Port GPIOB
#define nfcClk_Pin GPIO_PIN_13
#define nfcClk_GPIO_Port GPIOB
#define nfcMISO_Pin GPIO_PIN_14
#define nfcMISO_GPIO_Port GPIOB
#define nfcMOSI_Pin GPIO_PIN_15
#define nfcMOSI_GPIO_Port GPIOB
#define But_1_Pin GPIO_PIN_6
#define But_1_GPIO_Port GPIOC
#define But_2_Pin GPIO_PIN_7
#define But_2_GPIO_Port GPIOC
#define But_3_Pin GPIO_PIN_8
#define But_3_GPIO_Port GPIOC
#define But_4_Pin GPIO_PIN_9
#define But_4_GPIO_Port GPIOC
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define Flash_CLK_Pin GPIO_PIN_10
#define Flash_CLK_GPIO_Port GPIOC
#define Flash_MISO_Pin GPIO_PIN_11
#define Flash_MISO_GPIO_Port GPIOC
#define Flash_MOSI_Pin GPIO_PIN_12
#define Flash_MOSI_GPIO_Port GPIOC
#define SWO_Pin GPIO_PIN_3
#define SWO_GPIO_Port GPIOB
#define Flash_CS_Pin GPIO_PIN_5
#define Flash_CS_GPIO_Port GPIOB
#define Flash_HOLD_Pin GPIO_PIN_6
#define Flash_HOLD_GPIO_Port GPIOB
#define Flash_WP_Pin GPIO_PIN_7
#define Flash_WP_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
#define DEVBOARD
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
