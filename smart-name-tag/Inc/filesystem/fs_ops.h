/*
 * fsOps.h
 *
 *  Created on: Mar 1, 2020
 *      Author: lorenzo
 */

#ifndef FSOPS_H_
#define FSOPS_H_

#include <stdio.h>
#include "typedefs.h"
#include "nfc.h"
#include "fatfs.h"

#define CHECK(res) if((res != FS_OK) && (res != FS_DIR_CREATED)){break;}

typedef enum {
	FS_OK = 0,
	FS_MOUNT_ERR,
	FS_MKFS_ERR,
	FS_STAT_ERR,
	FS_DIR_CREATE_ERR,
	FS_DIR_CHANGE_ERR,
	FS_DIR_CREATED,
	FS_OPEN_ERR,
	FS_CLOSE_ERR,
	FS_READ_ERR,
	FS_WRITE_ERR,
	FS_MISMATCHED_DIM,
	FS_FULL
} FS_RESULT;

FS_RESULT createFS();  // format (if needed), mount, create base files/folders
FS_RESULT loadImages(u8** top_im, u8** bot_im, int* dim);
FS_RESULT loadVCF(u8 **buf, uint *size);  // read VCF from flash, load into buffer
FS_RESULT writeVcf();  // read from VCF rx buffer, write to flash

FS_RESULT mount();

FS_RESULT chdir(const char* path);

FS_RESULT home();

FS_RESULT fsize(const char* path, FSIZE_t* size);

FS_RESULT write(const char* path, const u8* buf, u16 size);

FS_RESULT read(const char* path, u8* buf, FSIZE_t size);

#endif /* FSOPS_H_ */
